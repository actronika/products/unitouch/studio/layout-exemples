# Custom Layout - Exemples
This repository contains exemple layouts for Unitouch Studio and documentation on how to write your own.

- CubeLayout.json: exemple layout for a cube, to be modified to match your own layout.
- Actuator Layouts Guide.pdf: detailed documentation explaining how json files are structured to enable you writing the layout for your own custom device.